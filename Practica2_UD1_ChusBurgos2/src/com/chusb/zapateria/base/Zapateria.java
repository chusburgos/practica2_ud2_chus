package com.chusb.zapateria.base;

import sun.util.resources.cldr.xog.LocaleNames_xog;

import java.time.LocalDate;

public abstract class Zapateria {

    private int referencia;
    private String marca;
    private String modelo;
    private int talla;
    private String color;
    private LocalDate fechaEntrada;
    private Float precio;

    public Zapateria(int referencia, String marca, String modelo, int talla, String color, LocalDate fechaEntrada, Float precio) {
        this.referencia = referencia;
        this.marca = marca;
        this.modelo = modelo;
        this.talla = talla;
        this.color = color;
        this.fechaEntrada = fechaEntrada;
        this.precio = precio;
    }

    public Zapateria() {}

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public int getTalla() {
        return talla;
    }

    public void setTalla(int talla) {
        this.talla = talla;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getReferencia() {
        return referencia;
    }

    public void setReferencia(int referencia) {
        this.referencia = referencia;
    }

    public LocalDate getFechaEntrada() {
        return fechaEntrada;
    }

    public void setFechaEntrada(LocalDate fechaEntrada) {
        this.fechaEntrada = fechaEntrada;
    }

    public Float getPrecio() {
        return precio;
    }

    public void setPrecio(Float precio) {
        this.precio = precio;
    }
}
