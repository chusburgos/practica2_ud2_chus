package com.chusb.zapateria.base;

import java.time.LocalDate;

public class Mocasines extends Zapateria {

    private String material;

    public Mocasines() {
        super();
    }

    public Mocasines(int referencia, String marca, String modelo, int talla, String color, LocalDate fechaEntrada,
                     Float precio, String material) {
        super(referencia, marca, modelo, talla, color, fechaEntrada, precio);
        this.material = material;
    }

    @Override
    public String toString() {
        return "-Mocasines- " + " Referencia: " + getReferencia() + "Marca: " + getMarca() + ", Modelo: " + getModelo() +
                ", Talla " + getTalla() + ", Fecha de entrada: " + getFechaEntrada() + ", Precio: " + getPrecio() +
                ", Color: " + getColor() + ", Material:" + material + ".";
    }

    public String getMaterial() {
        return material;
    }

    public void setMaterial(String material) {
        this.material = material;
    }
}
