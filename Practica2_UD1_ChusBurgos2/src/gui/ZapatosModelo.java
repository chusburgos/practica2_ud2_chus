package gui;

import com.chusb.zapateria.base.Botas;
import com.chusb.zapateria.base.Deportivas;
import com.chusb.zapateria.base.Mocasines;
import com.chusb.zapateria.base.Zapateria;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;

public class ZapatosModelo {
    private ArrayList<Zapateria> listaZapatos;

    public ZapatosModelo() {
        listaZapatos = new ArrayList<Zapateria>();
    }

    public ArrayList<Zapateria> obtenerZapatos() {
        return listaZapatos;
    }

    public void altaDeportivas(int referencia, String modelo, String marca, int talla, String color, LocalDate fechaEntrada,
                               Float precio, String tipoDeporte) {
        Deportivas nuevasDeportivas = new Deportivas(referencia, modelo, marca, talla, color, fechaEntrada, precio, tipoDeporte);
        listaZapatos.add(nuevasDeportivas);
    }

    public void altaBotas(int referencia, String modelo, String marca, int talla, String color, LocalDate fechaEntrada,
                          Float precio, String estilo) {
        Botas nuevasBotas = new Botas(referencia, modelo, marca, talla, color, fechaEntrada, precio, estilo);
        listaZapatos.add(nuevasBotas);
    }

    public void altaMocasines(int referencia, String modelo, String marca, int talla, String color, LocalDate fechaEntrada,
                          Float precio, String material) {
        Mocasines nuevosMocasines = new Mocasines(referencia, modelo, marca, talla, color, fechaEntrada, precio, material);
        listaZapatos.add(nuevosMocasines);
    }

    public boolean existeReferencia(int referencia) {
        for (Zapateria unZapato : listaZapatos) {
            if(unZapato.getReferencia() == (referencia)) {
                return true;
            }
        }
        return false;
    }


    public void exportarXML(File fichero) throws ParserConfigurationException, TransformerException {

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        DOMImplementation dom = builder.getDOMImplementation();
        Document documento = dom.createDocument(null, "xml", null);

        //Añado el nodo raiz - la primera etiqueta que contiene a las demas
        Element raiz = documento.createElement("Zapateria");
        documento.getDocumentElement().appendChild(raiz);

        Element nodoZapato = null, nodoDatos = null;
        Text texto = null;

        for (Zapateria unZapato : listaZapatos) {

            /*Añado dentro de la etiqueta raiz (Zapateria) una etiqueta
            dependiendo del tipo de zapato que este almacenando
            (deportivos, botas o mocasines)
             */
            if (unZapato instanceof Deportivas) {
                nodoZapato = documento.createElement("Deportivas");

            } else if(unZapato instanceof Botas) {
                nodoZapato = documento.createElement("Botas");
            }
            else {
                if(unZapato instanceof Mocasines) {
                    nodoZapato = documento.createElement("Mocasines");
                }
            }
            raiz.appendChild(nodoZapato);

            /*Dentro de la etiqueta zapateria le añado
            las subetiquetas con los datos de sus
            atributos (referencia, modelo, marca...)
             */
            nodoDatos = documento.createElement("referencia");
            nodoZapato.appendChild(nodoDatos);
            texto = documento.createTextNode(String.valueOf(((Zapateria) unZapato).getReferencia()));
            nodoDatos.appendChild(texto);

            nodoDatos = documento.createElement("modelo");
            nodoZapato.appendChild(nodoDatos);
            texto = documento.createTextNode(unZapato.getModelo());
            nodoDatos.appendChild(texto);

            nodoDatos = documento.createElement("marca");
            nodoZapato.appendChild(nodoDatos);
            texto = documento.createTextNode(unZapato.getMarca());
            nodoDatos.appendChild(texto);

            nodoDatos = documento.createElement("talla");
            nodoZapato.appendChild(nodoDatos);
            texto = documento.createTextNode(String.valueOf(((Zapateria) unZapato).getTalla()));
            nodoDatos.appendChild(texto);

            nodoDatos = documento.createElement("color");
            nodoZapato.appendChild(nodoDatos);
            texto = documento.createTextNode(unZapato.getColor());
            nodoDatos.appendChild(texto);

            nodoDatos = documento.createElement("fecha-entrada");
            nodoZapato.appendChild(nodoDatos);
            texto = documento.createTextNode(unZapato.getFechaEntrada().toString());
            nodoDatos.appendChild(texto);

            nodoDatos = documento.createElement("precio");
            nodoZapato.appendChild(nodoDatos);
            texto = documento.createTextNode(String.valueOf(((Zapateria)unZapato).getPrecio()));
            nodoDatos.appendChild(texto);


            /* Como hay un dato que depende del tipo de vehiculo
            debo acceder a él controlando el tipo de objeto
             */
            if (unZapato instanceof Deportivas) {
                nodoDatos = documento.createElement("tipo-deportivas");
                nodoZapato.appendChild(nodoDatos);
                texto = documento.createTextNode(((Deportivas) unZapato).getTipoDeporte());
                nodoDatos.appendChild(texto);
            }
            else if(unZapato instanceof Botas) {
                nodoDatos = documento.createElement("estilo");
                nodoZapato.appendChild(nodoDatos);
                texto = documento.createTextNode(((Botas) unZapato).getEstilo());
                nodoDatos.appendChild(texto);
            }
            else {
                if(unZapato instanceof Mocasines) {
                    nodoDatos = documento.createElement("material");
                    nodoZapato.appendChild(nodoDatos);
                    texto = documento.createTextNode(((Mocasines) unZapato).getMaterial());
                    nodoDatos.appendChild(texto);
                }
            }
        }

        /*
        Guardo los datos en "fichero" que es el objeto File
        recibido por parametro
         */
        Source source = new DOMSource(documento);
        Result resultado = new StreamResult(fichero);

        Transformer transformer = TransformerFactory.newInstance().newTransformer();
        transformer.transform(source, resultado);

    }

    public void importarXML(File fichero) throws ParserConfigurationException, IOException, SAXException {
        listaZapatos = new ArrayList<Zapateria>();
        Deportivas nuevasDeportivas = null;
        Botas nuevasBotas = null;
        Mocasines nuevosMocasines = null;

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document documento = builder.parse(fichero);

        NodeList listaElementos = documento.getElementsByTagName("*");

        for (int i = 0; i < listaElementos.getLength(); i++) {
            Element nodoZapateria = (Element) listaElementos.item(i);


            if (nodoZapateria.getTagName().equals("Deportivas")) {
                nuevasDeportivas = new Deportivas();
                nuevasDeportivas.setReferencia(Integer.parseInt(nodoZapateria.getChildNodes().item(0).getTextContent()));
                nuevasDeportivas.setModelo(nodoZapateria.getChildNodes().item(1).getTextContent());
                nuevasDeportivas.setMarca(nodoZapateria.getChildNodes().item(2).getTextContent());
                nuevasDeportivas.setTalla(Integer.parseInt(nodoZapateria.getChildNodes().item(3).getTextContent()));
                nuevasDeportivas.setColor(nodoZapateria.getChildNodes().item(5).getTextContent());
                nuevasDeportivas.setFechaEntrada(LocalDate.parse(nodoZapateria.getChildNodes().item(6).getTextContent()));
                nuevasDeportivas.setTipoDeporte(nodoZapateria.getChildNodes().item(7).getTextContent());

                listaZapatos.add(nuevasDeportivas);

            }
            else if (nodoZapateria.getTagName().equals("Botas")) {
                nuevasBotas = new Botas();
                nuevasBotas.setReferencia(Integer.parseInt(nodoZapateria.getChildNodes().item(0).getTextContent()));
                nuevasBotas.setModelo(nodoZapateria.getChildNodes().item(1).getTextContent());
                nuevasBotas.setMarca(nodoZapateria.getChildNodes().item(2).getTextContent());
                nuevasBotas.setTalla(Integer.parseInt(nodoZapateria.getChildNodes().item(3).getTextContent()));
                nuevasBotas.setColor(nodoZapateria.getChildNodes().item(5).getTextContent());
                nuevasBotas.setFechaEntrada(LocalDate.parse(nodoZapateria.getChildNodes().item(6).getTextContent()));
                nuevasBotas.setEstilo(nodoZapateria.getChildNodes().item(7).getTextContent());

                listaZapatos.add(nuevasBotas);
            }
            else {
                if(nodoZapateria.getTagName().equals("Mocasines")) {
                    nuevosMocasines = new Mocasines();
                    nuevosMocasines.setReferencia(Integer.parseInt(nodoZapateria.getChildNodes().item(0).getTextContent()));
                    nuevosMocasines.setModelo(nodoZapateria.getChildNodes().item(1).getTextContent());
                    nuevosMocasines.setMarca(nodoZapateria.getChildNodes().item(2).getTextContent());
                    nuevosMocasines.setTalla(Integer.parseInt(nodoZapateria.getChildNodes().item(3).getTextContent()));
                    nuevosMocasines.setColor(nodoZapateria.getChildNodes().item(5).getTextContent());
                    nuevosMocasines.setFechaEntrada(LocalDate.parse(nodoZapateria.getChildNodes().item(6).getTextContent()));
                    nuevosMocasines.setMaterial(nodoZapateria.getChildNodes().item(7).getTextContent());

                    listaZapatos.add(nuevasBotas);
                }
            }
        }


    }
}

