package gui;

import com.chusb.zapateria.base.Botas;
import com.chusb.zapateria.base.Deportivas;
import com.chusb.zapateria.base.Mocasines;
import com.chusb.zapateria.base.Zapateria;
import com.chusb.zapateria.util.Util;
import jdk.internal.org.xml.sax.SAXException;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.*;
import java.util.Properties;

public class ZapatosControlador implements ActionListener, ListSelectionListener, WindowListener {

    private Ventana ventana;
    private ZapatosModelo modelo;
    private File ultimaRutaExportada;

    public ZapatosControlador(Ventana ventana, ZapatosModelo modelo) {
        this.ventana = ventana;
        this.modelo = modelo;

        try {
            cargarDatosConfiguracion();
        } catch (IOException e) {
            e.printStackTrace();
        }

        addActionListener(this);
        addListSelectionListener(this);
        addWindowListener(this);

    }

    public ZapatosControlador(){
        super();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String actionCommand = e.getActionCommand();

        switch (actionCommand) {
            case "Nuevo":
                if (hayCamposVacios()) {
                    Util.mensajeError("Los siguientes campos no pueden estar vacios\n" +
                            "Referencia\nSexo\nMarca\nModelo\nTalla\nColor\nFecha de entrada\nCaracteristicas" +
                            ventana.tipoDepEstiloMaterialLbl.getText());
                    break;
                }

               else if (modelo.existeReferencia(ventana.txtReferencia.getText())) {
                    Util.mensajeError("Ya existe un producto con esa referencia\n+" +
                            ventana.txtReferencia.getText());
                    break;
                }

               else if (ventana.deportivoBtn.isSelected()){
                    modelo.altaDeportivas(Integer.parseInt(ventana.txtReferencia.getText()),
                            ventana.txtSexo.getText(),
                            ventana.txtMarca.getText(),
                            (String) ventana.comboBox1.getSelectedItem(),
                            ventana.txtColor.getText(),
                            ventana.fechaEntradaPicker.getDate(),
                            Float.parseFloat(ventana.txtPrecio.getText()),
                            ventana.tipoDepEstiloMaterialTxt.getText());
               }

               else if(ventana.botasBtn.isSelected()){
                    modelo.altaBotas(Integer.parseInt(ventana.txtReferencia.getText()),
                        ventana.txtSexo.getText(),
                        ventana.txtMarca.getText(),
                        (String) ventana.comboBox1.getSelectedItem(),
                        ventana.txtColor.getText(),
                        ventana.fechaEntradaPicker.getDate(),
                        Float.parseFloat(ventana.txtPrecio.getText()),
                        ventana.tipoDepEstiloMaterialTxt.getText());
               }

               else {
                    if(ventana.mocasinBtn.isSelected()) {
                        modelo.altaMocasines(Integer.parseInt(ventana.txtReferencia.getText()),
                                ventana.txtSexo.getText(),
                                ventana.txtMarca.getText(),
                                (String) ventana.comboBox1.getSelectedItem(),
                                ventana.txtColor.getText(),
                                ventana.fechaEntradaPicker.getDate(),
                                Float.parseFloat(ventana.txtPrecio.getText()),
                                ventana.tipoDepEstiloMaterialTxt.getText());
                    }
               }

            limpiarCampos();
            refrescar();
            break;

            case "Importar":
                JFileChooser selectorFichero = Util.crearSelectorFicheros(ultimaRutaExportada, "Archivo XML", "xml");
                int opt = selectorFichero.showOpenDialog(null);
                if (opt == JFileChooser.APPROVE_OPTION) {
                    try {
                        modelo.importarXML(selectorFichero.getSelectedFile());
                    } catch (ParserConfigurationException ex) {
                        ex.printStackTrace();
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    } catch (org.xml.sax.SAXException ex) {
                        ex.printStackTrace();
                    }
                    refrescar();
                }
                break;

            case "Exportar":
                JFileChooser selectorFichero2 = Util.crearSelectorFicheros(ultimaRutaExportada, "Archivos XML", "xml");
                int opt2 = selectorFichero2.showSaveDialog(null);
                if (opt2 == JFileChooser.APPROVE_OPTION) {
                    try {
                        modelo.exportarXML(selectorFichero2.getSelectedFile());
                        actualizarDatosConfiguracion(selectorFichero2.getSelectedFile());
                    } catch (ParserConfigurationException ex) {
                        ex.printStackTrace();
                    } catch (TransformerException ex) {
                        ex.printStackTrace();
                    }
                }
                break;

            case "Deportivos":
                ventana.tipoDepEstiloMaterialLbl.setText("Tipo de deporte");
                break;

            case "Botas":
                ventana.tipoDepEstiloMaterialLbl.setText("Estilo");
                break;

            case "Mocasines":
                ventana.tipoDepEstiloMaterialLbl.setText("Material");
                break;
        }

    }

    private boolean hayCamposVacios() {
        if (ventana.tipoDepEstiloMaterialTxt.getText().isEmpty() ||
                ventana.txtReferencia.getText().isEmpty() ||
                ventana.txtSexo.getText().isEmpty() ||
                ventana.txtMarca.getText().isEmpty() ||
                ventana.txtPrecio.getText().isEmpty() ||
                ventana.txtColor.getText().isEmpty() ||
                ventana.fechaEntradaPicker.getText().isEmpty()) {
            return true;
        }
        return false;
    }


    private void limpiarCampos() {
        ventana.txtReferencia.requestFocus();
        ventana.txtSexo.setText(null);
        ventana.txtMarca.setText(null);
        ventana.txtColor.setText(null);
        ventana.fechaEntradaPicker.setText(null);
        ventana.txtPrecio.setText(null);
    }

    private void refrescar() {
        ventana.dlmZapateria.clear();
        for (Zapateria unZapato : modelo.obtenerZapatos()) {
            ventana.dlmZapateria.addElement(unZapato);
        }
    }



    private void refrescarComboBox() {
        ventana.dcbm.removeAllElements();
        for (Zapateria unZapato : ventana.lista) {
            ventana.dcbm.addElement(unZapato);
        }
    }

    private void addActionListener(ActionListener listener) {
        ventana.deportivoBtn.addActionListener(listener);
        ventana.botasBtn.addActionListener(listener);
        ventana.mocasinBtn.addActionListener(listener);
        ventana.exportarBtn.addActionListener(listener);
        ventana.importarBtn.addActionListener(listener);
        ventana.nuevoBtn.addActionListener(listener);
    }

    private void addWindowListener(WindowListener listener) {
        ventana.frame.addWindowListener(listener);
    }

    private void addListSelectionListener(ListSelectionListener listener) {
        ventana.list1.addListSelectionListener(listener);
    }

    private void cargarDatosConfiguracion() throws IOException {
        Properties configuracion = new Properties();
        configuracion.load(new FileReader("Zapateria.conf"));
        ultimaRutaExportada = new File(configuracion.getProperty("ultimaRutaExportada"));
    }

    private void actualizarDatosConfiguracion(File ultimaRutaExportada) {
        this.ultimaRutaExportada = ultimaRutaExportada;
    }

    private void guardarConfiguracion() throws IOException {
        Properties configuracion = new Properties();
        configuracion.setProperty("ultimaRutaExportada", ultimaRutaExportada.getAbsolutePath());
        configuracion.store(new PrintWriter("Zapateria.conf"), "Datos configuracion Zapateria");

    }


    @Override
    public void valueChanged(ListSelectionEvent e) {
        if (e.getValueIsAdjusting()) {
            Zapateria zapatoSeleccionado = (Zapateria) ventana.list1.getSelectedValue();
            ventana.txtReferencia.setText(String.valueOf(((Zapateria) zapatoSeleccionado).getReferencia()));
            ventana.txtSexo.setText(zapatoSeleccionado.getSexo());
            ventana.txtMarca.setText(zapatoSeleccionado.getMarca());
            ventana.comboBox1.setSelectedItem(String.valueOf((Zapateria) zapatoSeleccionado).toString());
            ventana.txtColor.setText(zapatoSeleccionado.getColor());
            ventana.fechaEntradaPicker.setDate(zapatoSeleccionado.getFechaEntrada());
            ventana.txtPrecio.setText(String.valueOf(((Zapateria) zapatoSeleccionado).getPrecio()));

            if (zapatoSeleccionado instanceof Deportivas) {
                ventana.deportivoBtn.doClick();
                ventana.tipoDepEstiloMaterialTxt.setText(((Deportivas) zapatoSeleccionado).getTipoDeporte());
            }
            else if(zapatoSeleccionado instanceof Botas) {
                ventana.botasBtn.doClick();
                ventana.tipoDepEstiloMaterialTxt.setText(((Botas) zapatoSeleccionado).getEstilo());
            }
            else if(zapatoSeleccionado instanceof Mocasines) {
                ventana.mocasinBtn.doClick();
                ventana.tipoDepEstiloMaterialTxt.setText(((Mocasines) zapatoSeleccionado).getMaterial());
            }
        }
    }


    @Override
    public void windowClosing(WindowEvent e) {
        int resp = Util.mensajeConfirmacion("¿Desea salir de la ventana?", "Salir");
        if (resp == JOptionPane.OK_OPTION) {
            try {
                guardarConfiguracion();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            System.exit(0);
        }

    }

    @Override
    public void windowOpened(WindowEvent windowEvent) {

    }

    @Override
    public void windowClosed(WindowEvent windowEvent) {

    }

    @Override
    public void windowIconified(WindowEvent windowEvent) {

    }

    @Override
    public void windowDeiconified(WindowEvent windowEvent) {

    }

    @Override
    public void windowActivated(WindowEvent windowEvent) {

    }

    @Override
    public void windowDeactivated(WindowEvent windowEvent) {

    }


}
