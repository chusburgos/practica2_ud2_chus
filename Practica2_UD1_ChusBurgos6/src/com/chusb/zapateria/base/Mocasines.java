package com.chusb.zapateria.base;

import java.time.LocalDate;

public class Mocasines extends Zapateria {
    /**
     * Mocasines es una de las tres clases hijas que se han creado, extiende de la clase padre que es Zapateria, es decir
     * los parametros que tiene la clase Padre, Zapateria, Mocasines tambien los tiene.
     */
    private String material;
    /**
     * Constructor vacio, el super significa que hereda los parametros pero estan vacios.
     */
    public Mocasines() {
        super();
    }
    /**
     * @param referencia Refencia del producto, solo se introduciran valores numericos.
     * @param sexo Sexo del cliente, sera introducido por teclado
     * @param marca Marca del zapato que vaya a comprar el cliente
     * @param talla Talla que utilice el cliente
     * @param color Color a gusto del cliente
     * @param fechaEntrada Fecha de entrada sera, la entrada del cliente a la aplicación
     * @param precio Precio equivalente al modelo y marca del producto
     *
     *  En la clase Mocasines se añadira el parametro material
     * @param material material como piel, polipiel...
     *
     * Creacion del constructor que devolvera todos los valores que se introduciran por teclado
     * incluyendo material
     */
    public Mocasines(int referencia, String sexo, String marca, String talla, String color, LocalDate fechaEntrada,
                     Float precio, String material) {
        super(referencia, sexo, marca, talla, color, fechaEntrada, precio);
        this.material = material;
    }
    /**
     * Metodo toString devuelve todos los valores añadidos por teclado una vez el cliente los haya metido,
     * los datos se monstraran en ese orden y esa estetica.
     *
     */
    @Override
    public String toString() {
        return "- Mocasines - " + " Referencia: " + getReferencia()+ ", Sexo: " + getSexo() + " Marca: " + getMarca() +
                ", Talla " + getTalla() + ", Fecha de entrada: " + getFechaEntrada() + ", Precio: " + getPrecio() +
                ", Color: " + getColor() + ", Material:" + material + ".";
    }

    /**
     * Metodo que devuelve el material de los mocasines
     * @return material
     */
    public String getMaterial() {
        return material;
    }
    /**
     * Metodo que le asigna el material al producto(Zapato)
     * @param material material que sera asignado
     */
    public void setMaterial(String material) {
        this.material = material;
    }
}
