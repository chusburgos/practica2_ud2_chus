package com.chusb.zapateria.base;

import java.time.LocalDate;

public class Botas extends Zapateria {

    /**
     * Botas es una de las tres clases hijas que se han creado, extiende de la clase padre que es Zapateria, es decir
     * los parametros que tiene la clase Padre, Zapateria, Botas tambien los tiene.
     *
     */
    private String estilo;

    /**
     * Constructor vacio, el super significa que hereda los parametros pero estan vacios.
     */
    public Botas() {
            super();
    }

    /**
     * @param referencia Refencia del producto, solo se introduciran valores numericos.
     * @param sexo Sexo del cliente, sera introducido por teclado
     * @param marca Marca del zapato que vaya a comprar el cliente
     * @param talla Talla que utilice el cliente
     * @param color Color a gusto del cliente
     * @param fechaEntrada Fecha de entrada sera, la entrada del cliente a la aplicación
     * @param precio Precio equivalente al modelo y marca del producto
     *
     *  En la clase Botas se añadira el parametro estilo.
     * @param estilo Estilo de la bota ya sea de vestir, estilo militar, con tacon, platadorma...
     *
     * Creacion del constructor que devolvera todos los valores que se introduciran por teclado
     * incluyendo estilo
     */

    public Botas(int referencia, String sexo, String marca, String talla, String color, LocalDate fechaEntrada,
                Float precio, String estilo) {
            super(referencia, sexo, marca, talla, color, fechaEntrada, precio);
            this.estilo = estilo;
    }

    /**
     * Metodo toString devuelve todos los valores añadidos por teclado una vez el cliente los haya metido,
     * los datos se monstraran en ese orden y esa estetica.
     *
     */
    @Override
    public String toString() {
        return "- Botas - " + " Referencia: " + getReferencia() + ", Sexo: " + getSexo() +", Marca: " + getMarca() +
               ", Talla " + getTalla() + ", Fecha de entrada: " + getFechaEntrada() + ", Precio: " + getPrecio() +
               ", Color: " + getColor() + ", Estilo:" + estilo + ".";
    }

    /**
     * Metodo que devuelve el estilo de las botas
     * @return estilo Estilo de las botas
     */
    public String getEstilo() {
        return estilo;
    }

    /**
     * Metodo que le asigna el estilo de las botas
     * @param estilo estilo de las botas
     */
    public void setEstilo(String estilo) {
        this.estilo = estilo;
    }
}
