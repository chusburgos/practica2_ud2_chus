package com.chusb.zapateria.base;

import java.time.LocalDate;

public abstract class Zapateria {

    /**
     * En la clase Zapateria se crean todas las variables que se van a utilizar en las clases hijas como
     * Deportivas, Botas y Mocasines, ya que Zapateria es la clase padre.
     *
     */
    private int referencia;
    private String sexo;
    private String marca;
    private String talla;
    private String color;
    private LocalDate fechaEntrada;
    private Float precio;

    /**
     * Creación de un constructor lleno, que sera pasado por teclado en la aplicacion por el propio cliente
     *
     * @param referencia Refencia del producto, solo se introduciran valores numericos.
     * @param sexo Sexo del cliente, sera introducido por teclado
     * @param marca Marca del zapato que vaya a comprar el cliente
     * @param talla Talla que utilice el cliente
     * @param color Color a gusto del cliente
     * @param fechaEntrada Fecha de entrada sera, la entrada del cliente a la aplicación
     * @param precio Precio equivalente al modelo y marca del producto
     */
    public Zapateria(int referencia, String sexo, String marca, String talla, String color, LocalDate fechaEntrada, Float precio) {
        this.referencia = referencia;
        this.sexo = sexo;
        this.marca = marca;
        this.talla = talla;
        this.color = color;
        this.fechaEntrada = fechaEntrada;
        this.precio = precio;
    }

    /**
     * Constructor vacio por si en algun momento se necesita el metodo vacio, al crear o dar de alta un
     * modelo de zapato
     */
    public Zapateria() {}

    /**
     * Se generan los getters y setters de todos los parametros que se han declarado.
     *
     * Metodo que devuelve la referencia del producto asignado
     * @return decuelve la referencia
     */
    public int getReferencia() {
        return referencia;
    }

    /**
     * Metodo que le asigna referencia a un producto(Zapato)
     * @param referencia numero de referencia
     */
    public void setReferencia(int referencia) {
        this.referencia = referencia;
    }

    /**
     * Metodo que devuelve el sexo del cliente
     * @return sexo devuelve el sexo
     */
    public String getSexo() {
        return sexo;
    }

    /**
     * Metodo que le asigna el sexo al cliente
     * @param sexo sexo del cliente
     */
    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    /**
     * Metodo que devuelve la marca del producto(Zapato)
     * @return marca Marca del producto
     */
    public String getMarca() {
        return marca;
    }

    /**
     * Metodo que asigna la marca del producto(Zapato)
     * @param marca Marca del producto
     */
    public void setMarca(String marca) {
        this.marca = marca;
    }

    /**
     * Metodo que devuelve la talla del producto(Zapato)
     * @return  Marca del producto
     */
    public String getTalla() {
        return talla;
    }
    /**
     * Metodo que asigna la talla del producto(Zapato)
     * @param talla Marca del producto
     */
    public void setTalla(String talla) {
        this.talla = talla;
    }
    /**
     * Metodo que devuelve la talla del producto(Zapato)
     * @return  Marca del producto
     */
    public String getColor() {
        return color;
    }
    /**
     * Metodo que asigna el color del producto(Zapato)
     * @param color Marca del producto
     */
    public void setColor(String color) {
        this.color = color;
    }

    /**
     * Metodo que devuelve la fecha de entrada de registro del producto(Zapato) por el cliente
     * @return  fechaEntrada del producto
     */
    public LocalDate getFechaEntrada() {
        return fechaEntrada;
    }
    /**
     * Metodo que asigna la Fecha de Entrada del producto(Zapato)
     * @param fechaEntrada Fecha de Entrada del producto
     */
    public void setFechaEntrada(LocalDate fechaEntrada) {
        this.fechaEntrada = fechaEntrada;
    }
    /**
     * Metodo que devuelve el precio del producto(Zapato)
     * @return precio del producto
     */
    public Float getPrecio() {
        return precio;
    }
    /**
     * Metodo que asigna el precio del producto(Zapato)
     * @param precio Precio del producto
     */
    public void setPrecio(Float precio) {
        this.precio = precio;
    }

}
