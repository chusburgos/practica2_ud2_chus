package com.chusb.zapateria.base;

import javax.swing.*;
import java.time.LocalDate;

public class Deportivas extends Zapateria {

    /**
     * Deportivas es una de las tres clases hijas que se han creado, extiende de la clase padre que es Zapateria, es decir
     * los parametros que tiene la clase Padre, Zapateria, Deportivas tambien los tiene.
     */
    private String tipoDeporte;

    /**
     * Constructor vacio, el super significa que hereda los parametros pero estan vacios.
     */
    public Deportivas() {
        super();
    }

    /**
     * @param referencia Refencia del producto, solo se introduciran valores numericos.
     * @param sexo Sexo del cliente, sera introducido por teclado
     * @param marca Marca del zapato que vaya a comprar el cliente
     * @param talla Talla que utilice el cliente
     * @param color Color a gusto del cliente
     * @param fechaEntrada Fecha de entrada sera, la entrada del cliente a la aplicación
     * @param precio Precio equivalente al modelo y marca del producto
     *
     *  En la clase Deportivas se añadira el parametro tipo de deporte.
     * @param tipoDeporte Tipo de deporte puede ser running, ciclismo, tenis, mas de sport...
     *
     * Creacion del constructor que devolvera todos los valores que se introduciran por teclado
     * incluyendo tipoDeporte
     */
    public Deportivas(int referencia, String sexo, String marca, String talla, String color, LocalDate fechaEntrada,
                      Float precio, String tipoDeporte) {
       super(referencia, sexo, marca, talla, color, fechaEntrada, precio);
       this.tipoDeporte = tipoDeporte;
    }

    /**
     * Metodo toString devuelve todos los valores añadidos por teclado una vez el cliente los haya metido,
     * los datos se monstraran en ese orden y esa estetica.
     *
     */
    @Override
    public String toString() {
        return "- Deportivas - " + " Referencia: " + getReferencia()+ ", Sexo: " + getSexo() + " Marca: " + getMarca() +
                ", Talla " + getTalla() + ", Fecha de entrada: " + getFechaEntrada() + ", Precio: " + getPrecio() +
                 ", Color: " + getColor() + ", Tipo de deporte:" + tipoDeporte + ".";
    }
    /**
     * Metodo que devuelve el tipo de deporte de las deportivas
     * @return tipoDeporte el tipo de deporte de las deportivas
     */
    public String getTipoDeporte() {
        return tipoDeporte;
    }
    /**
     * Metodo que le asigna el tipo de deporte
     * @param tipoDeporte el tipo de deporte
     */
    public void setTipoDeporte(String tipoDeporte) {
        this.tipoDeporte = tipoDeporte;
    }
}
