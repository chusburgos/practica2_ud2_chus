package gui;

import com.chusb.zapateria.base.Zapateria;
import com.github.lgooddatepicker.components.DatePicker;
import javafx.scene.input.InputMethodTextRun;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.LinkedList;

public class Ventana {
    /**
     * En la clase Ventana.form se crea un cuadro que sera donde se empiece a realizar toda la interfaz de la aplicacion
     * por muy sencilla que sea. Dentro del cuadro se pondra un panel y ahí sera donde se añada las casillas de texto,
     * los diferentes botones, comboBox, DatePicker(la fecha de registro), el listado una vez se hayan introducido varios
     * productos y etiquetas, que depenpendiendo que modelo de zapato es variara la opcion de introducir los datos.
     */
    /**
     * Conforme se añaden los parametros en la clase Ventana.form se van declarando en esta clase (Ventana.java)
     */
    ZapatosControlador zapato = new ZapatosControlador();
    private JPanel Panel1;
    public JFrame frame;

    public JRadioButton deportivoBtn;
    public JRadioButton botasBtn;
    public JRadioButton mocasinBtn;

    public JButton importarBtn;
    public JButton nuevoBtn;
    public JButton exportarBtn;

    public JTextField txtMarca;
    public JTextField txtSexo;
    public JTextField txtColor;
    public DatePicker fechaEntradaPicker;
    public JTextField txtReferencia;
    public JTextField tipoDepEstiloMaterialTxt;
    public JTextField txtPrecio;

    public JList list1;
    public JLabel tipoDepEstiloMaterialLbl;
    public DefaultListModel<Zapateria> dlmZapateria;
    public JComboBox comboBox1;
    public DefaultComboBoxModel<Zapateria> dcbm;
    public LinkedList<Zapateria> lista;

    /**
     * Metodo que crea una ventana llamada Zapateria, donde se añade el panel se le van asignando una serie de normas,
     * como que cuando se le de la cruz la ventana se cierre. La ventana sera visible y coge todos los paquetes
     * para su funcionalidad, la localidad sera centrada
     */
    public Ventana() {
        frame = new JFrame("Zapateria");
        frame.setContentPane(Panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        frame.setLocationRelativeTo(null);

        initComplements();
        anadirDatosComboBox();
    }

    /**
     * Metodo que inicia los complementos de la clase padre(Zapateria), creando un ArrayList de esta clase
     * y añadiento a la lista cada modelo. Es decir cogera cada uno de los parametros de Zapateria y los mostrara en
     * la lista una vez sean añadidos por el cliente.
     */
    private void initComplements() {
        dlmZapateria = new DefaultListModel<Zapateria>();
        list1.setModel(dlmZapateria);
    }

    /**
     * Metodo que añade los datos al comboBox, los datos seran siempre los mismos, en orden ascendente.
     * Una vez creado el comboBox se añade el valor a este, tantas veces se quiera.
     */
    public void anadirDatosComboBox() {
        comboBox1.addItem("36");
        comboBox1.addItem("37");
        comboBox1.addItem("38");
        comboBox1.addItem("39");
        comboBox1.addItem("40");
        comboBox1.addItem("41");
        comboBox1.addItem("42");
        comboBox1.addItem("43");
        comboBox1.addItem("44");
    }
}
