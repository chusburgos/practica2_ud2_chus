package com.chusb.zapateria.util;

import gui.Ventana;
import gui.ZapatosControlador;
import gui.ZapatosModelo;

public class Principal {
    public static void main(String[] args) {
        Ventana ventana = new Ventana();
        ZapatosModelo modelo = new ZapatosModelo();
        ZapatosControlador controlador = new ZapatosControlador(ventana, modelo);

    }
}
