package com.chusb.zapateria.base;

import java.time.LocalDate;

public abstract class Zapateria {

    private int referencia;
    private String sexo;
    private String marca;
    private String talla;
    private String color;
    private LocalDate fechaEntrada;
    private Float precio;

    public Zapateria(int referencia, String sexo, String marca, String talla, String color, LocalDate fechaEntrada, Float precio) {
        this.referencia = referencia;
        this.sexo = sexo;
        this.marca = marca;
        this.talla = talla;
        this.color = color;
        this.fechaEntrada = fechaEntrada;
        this.precio = precio;
    }

    public Zapateria() {}

    public int getReferencia() {
        return referencia;
    }
    public void setReferencia(int referencia) {
        this.referencia = referencia;
    }
    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }
    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }


    public String getTalla() {
        return talla;
    }

    public void setTalla(String talla) {
        this.talla = talla;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public LocalDate getFechaEntrada() {
        return fechaEntrada;
    }

    public void setFechaEntrada(LocalDate fechaEntrada) {
        this.fechaEntrada = fechaEntrada;
    }

    public Float getPrecio() {
        return precio;
    }

    public void setPrecio(Float precio) {
        this.precio = precio;
    }

}
