package com.chusb.zapateria.base;

import java.time.LocalDate;

public class Botas extends Zapateria {

    private String estilo;

    public Botas() {
            super();
    }

    public Botas(int referencia, String sexo, String marca, String talla, String color, LocalDate fechaEntrada,
                Float precio, String estilo) {
            super(referencia, sexo, marca, talla, color, fechaEntrada, precio);
            this.estilo = estilo;
    }

    @Override
    public String toString() {
        return "-Botas- " + " Referencia: " + getReferencia() + ", Sexo: " + getSexo() +"Marca: " + getMarca() +
               ", Talla " + getTalla() + ", Fecha de entrada: " + getFechaEntrada() + ", Precio: " + getPrecio() +
               ", Color: " + getColor() + ", Estilo:" + estilo + ".";
    }

    public String getEstilo() {
        return estilo;
    }

    public void setEstilo(String estilo) {
        this.estilo = estilo;
    }
}
