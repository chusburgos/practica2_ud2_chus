package com.chusb.zapateria.base;

import javax.swing.*;
import java.time.LocalDate;

public class Deportivas extends Zapateria {

    private String tipoDeporte;

    public Deportivas() {
        super();
    }

    public Deportivas(int referencia, String sexo, String marca, String talla, String color, LocalDate fechaEntrada,
                      Float precio, String tipoDeporte) {
       super(referencia, sexo, marca, talla, color, fechaEntrada, precio);
       this.tipoDeporte = tipoDeporte;
    }

    @Override
    public String toString() {
        return "-Deportivas- " + " Referencia: " + getReferencia()+ ", Sexo: " + getSexo() + "Marca: " + getMarca() +
                ", Talla " + getTalla() + ", Fecha de entrada: " + getFechaEntrada() + ", Precio: " + getPrecio() +
                 ", Color: " + getColor() + ", Tipo de deporte:" + tipoDeporte + ".";
    }

    public String getTipoDeporte() {
        return tipoDeporte;
    }

    public void setTipoDeporte(String tipoDeporte) {
        this.tipoDeporte = tipoDeporte;
    }
}
