package gui;

import com.chusb.zapateria.base.Zapateria;
import com.github.lgooddatepicker.components.DatePicker;
import javafx.scene.input.InputMethodTextRun;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.LinkedList;

public class Ventana {
    ZapatosControlador zapato = new ZapatosControlador();
    private JPanel Panel1;
    public JFrame frame;

    public JRadioButton deportivoBtn;
    public JRadioButton botasBtn;
    public JRadioButton mocasinBtn;

    public JButton importarBtn;
    public JButton nuevoBtn;
    public JButton exportarBtn;

    public JTextField txtMarca;
    public JTextField txtSexo;
    public JTextField txtColor;
    public DatePicker fechaEntradaPicker;
    public JTextField txtReferencia;
    public JTextField tipoDepEstiloMaterialTxt;
    public JTextField txtPrecio;

    public JList list1;
    public JLabel tipoDepEstiloMaterialLbl;
    public DefaultListModel<Zapateria> dlmZapateria;
    public JComboBox comboBox1;
    public DefaultComboBoxModel<Zapateria> dcbm;
    public LinkedList<Zapateria> lista;


    public Ventana() {
        frame = new JFrame("CalzaTendencias");
        frame.setContentPane(Panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        frame.setLocationRelativeTo(null);

        initComplements();
        anadirDatosComboBox();
    }

    private void initComplements() {
        dlmZapateria = new DefaultListModel<Zapateria>();
        list1.setModel(dlmZapateria);
    }

    public void anadirDatosComboBox() {
        comboBox1.addItem("36");
        comboBox1.addItem("37");
        comboBox1.addItem("38");
        comboBox1.addItem("39");
        comboBox1.addItem("40");
        comboBox1.addItem("41");
        comboBox1.addItem("42");
        comboBox1.addItem("43");
        comboBox1.addItem("44");
    }
}
