package gui;

import com.chusb.zapateria.base.Zapateria;
import com.github.lgooddatepicker.components.DatePicker;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class Ventana extends JDialog {
    private JPanel Panel1;
    private JFrame frame;

    private JRadioButton deportivoBtn;
    private JRadioButton botasBtn;
    private JRadioButton mocasinBtn;

    private JButton importarBtn;
    private JButton nuevoBtn;
    private JButton exportarBtn;

    private JTextField txtMarca;
    private JTextField txtTalla;
    private JTextField txtColor;
    private DatePicker fechaEntradaPicker;
    private JTextField txtReferencia;
    private JTextField tipoDepEstiloMaterialTxt;

    private JList list1;
    private JLabel tipoDepEstiloMaterialLbl;
    private JTextField txtSexo;


    public DefaultListModel<Zapateria> dlmZapateria;

    public Ventana() {
        frame = new JFrame("CalzaTendencias");
        frame.setContentPane(Panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        frame.setLocationRelativeTo(null);

        initComplements();
    }

    private void initComplements() {
        dlmZapateria = new DefaultListModel<Zapateria>();
        list1.setModel(dlmZapateria);
    }
}
