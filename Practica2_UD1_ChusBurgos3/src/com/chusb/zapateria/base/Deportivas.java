package com.chusb.zapateria.base;

import java.time.LocalDate;

public class Deportivas extends Zapateria {

    private String tipoDeporte;

    public Deportivas() {
        super();
    }

    public Deportivas(int referencia, String sexo, String modelo, String marca, int talla, String color, LocalDate fechaEntrada,
                      Float precio, String tipoDeporte) {
       super(referencia, sexo, marca, modelo, talla, color, fechaEntrada, precio);
       this.tipoDeporte = tipoDeporte;
    }

    @Override
    public String toString() {
        return "-Deportivas- " + " Referencia: " + getReferencia()+ ", Sexo: " + getSexo() + "Marca: " + getMarca() + ", Modelo: " + getModelo() +
                ", Talla " + getTalla() + ", Fecha de entrada: " + getFechaEntrada() + ", Precio: " + getPrecio() +
                 ", Color: " + getColor() + ", Tipo de deporte:" + tipoDeporte + ".";
    }

    public String getTipoDeporte() {
        return tipoDeporte;
    }

    public void setTipoDeporte(String tipoDeporte) {
        this.tipoDeporte = tipoDeporte;
    }
}
