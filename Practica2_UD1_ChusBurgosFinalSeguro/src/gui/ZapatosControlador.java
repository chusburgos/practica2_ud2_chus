package gui;

import com.chusb.zapateria.base.Botas;
import com.chusb.zapateria.base.Deportivas;
import com.chusb.zapateria.base.Mocasines;
import com.chusb.zapateria.base.Zapateria;
import com.chusb.zapateria.util.Util;
import jdk.internal.org.xml.sax.SAXException;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.*;
import java.util.Properties;

public class ZapatosControlador implements ActionListener, ListSelectionListener, WindowListener {

    /**
     * Se crean los parametros que se van a utilizar en esta clase y File que se escribira la ultima ruta exportada
     */
    private Ventana ventana;
    private ZapatosModelo modelo;
    private File ultimaRutaExportada;

    /**
     * Constructor que se pasan los valores de ventana y modelo. Ya que en la clase principal se necesitara añadirlos.
     * @param ventana
     * @param modelo
     */
    public ZapatosControlador(Ventana ventana, ZapatosModelo modelo) {
        this.ventana = ventana;
        this.modelo = modelo;

        try {
            cargarDatosConfiguracion();
        } catch (IOException e) {
            e.printStackTrace();
        }

        // se añade la accion de escuchar, es decir, que la ventana sea llamada y se muestre
        addActionListener(this);
        addListSelectionListener(this);
        addWindowListener(this);

    }

    /**
     * Contructor vacio, por si en algun momento se necesita un constructor sin parametros introducidos
     */
    public ZapatosControlador(){
        super();
    }

    /**
     * Metodo que hara la accion de los botones.
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        String actionCommand = e.getActionCommand();

        switch (actionCommand) {
            /** Cuando se pulsa el boton "Nuevo"
             *  podrar producirse diversas acciones. Si algun campo esta vacio nos lo hara saber con un mensaje
             *  Si la referencia ya existe tambien saldra un mensaje comunicandonslo.
             *  Y si no se produce ningun indicente de los anteriores, se dara de alta dependiendo del boton que se haya
             *  seleccionado, podra ser deportiva, bota o mocasin, añadiendo los parametros por escrito en la ventana
             */
            case "Nuevo":
                if (hayCamposVacios()) {
                    Util.mensajeError("Los siguientes campos no pueden estar vacios\n" +
                            "Referencia\nSexo\nMarca\nModelo\nTalla\nColor\nFecha de entrada\nCaracteristicas" +
                            ventana.tipoDepEstiloMaterialLbl.getText());
                    break;
                }

               else if (modelo.existeReferencia(ventana.txtReferencia.getText())) {
                    Util.mensajeError("Ya existe un producto con esa referencia\n+" +
                            ventana.txtReferencia.getText());
                    break;
                }

               else if (ventana.deportivoBtn.isSelected()){
                    modelo.altaDeportivas(Integer.parseInt(ventana.txtReferencia.getText()),
                            ventana.txtSexo.getText(),
                            ventana.txtMarca.getText(),
                            (String) ventana.comboBox1.getSelectedItem(),
                            ventana.txtColor.getText(),
                            ventana.fechaEntradaPicker.getDate(),
                            Float.parseFloat(ventana.txtPrecio.getText()),
                            ventana.tipoDepEstiloMaterialTxt.getText());
               }

               else if(ventana.botasBtn.isSelected()){
                    modelo.altaBotas(Integer.parseInt(ventana.txtReferencia.getText()),
                        ventana.txtSexo.getText(),
                        ventana.txtMarca.getText(),
                        (String) ventana.comboBox1.getSelectedItem(),
                        ventana.txtColor.getText(),
                        ventana.fechaEntradaPicker.getDate(),
                        Float.parseFloat(ventana.txtPrecio.getText()),
                        ventana.tipoDepEstiloMaterialTxt.getText());
               }

               else {
                    if(ventana.mocasinBtn.isSelected()) {
                        modelo.altaMocasines(Integer.parseInt(ventana.txtReferencia.getText()),
                                ventana.txtSexo.getText(),
                                ventana.txtMarca.getText(),
                                (String) ventana.comboBox1.getSelectedItem(),
                                ventana.txtColor.getText(),
                                ventana.fechaEntradaPicker.getDate(),
                                Float.parseFloat(ventana.txtPrecio.getText()),
                                ventana.tipoDepEstiloMaterialTxt.getText());
                    }
               }

            limpiarCampos();
            refrescar();
            break;

            /**
             * Cuando se pulsa el boton "Importar", se importan todos los valores en el fichero xml
             */
            case "Importar":
                JFileChooser selectorFichero = Util.crearSelectorFicheros(ultimaRutaExportada, "Archivo XML", "xml");
                int opt = selectorFichero.showOpenDialog(null);
                if (opt == JFileChooser.APPROVE_OPTION) {
                    try {
                        modelo.importarXML(selectorFichero.getSelectedFile());
                    } catch (ParserConfigurationException ex) {
                        ex.printStackTrace();
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    } catch (org.xml.sax.SAXException ex) {
                        ex.printStackTrace();
                    }
                    refrescar();
                }
                break;
            /**
             * Cuando se pulsa el boton "Exportar", se exportan todos los valores en el fichero xml
             */
            case "Exportar":
                JFileChooser selectorFichero2 = Util.crearSelectorFicheros(ultimaRutaExportada, "Archivos XML", "xml");
                int opt2 = selectorFichero2.showSaveDialog(null);
                if (opt2 == JFileChooser.APPROVE_OPTION) {
                    try {
                        modelo.exportarXML(selectorFichero2.getSelectedFile());
                        actualizarDatosConfiguracion(selectorFichero2.getSelectedFile());
                    } catch (ParserConfigurationException ex) {
                        ex.printStackTrace();
                    } catch (TransformerException ex) {
                        ex.printStackTrace();
                    }
                }
                break;

            /**
             * Cuando se pulsa el boton "Deportivos", "Botas" o "Mocasines", en se mostrara un texto
             * personalizado en las caracteristicas. Dependiendo del modelo elegido.
             */
            case "Deportivos":
                ventana.tipoDepEstiloMaterialLbl.setText("Tipo de deporte");
                break;

            case "Botas":
                ventana.tipoDepEstiloMaterialLbl.setText("Estilo");
                break;

            case "Mocasines":
                ventana.tipoDepEstiloMaterialLbl.setText("Material");
                break;
        }

    }

    /**
     * Metodo que devuelve el valor del campo vacio, si un campo de texto no ha sido introducido o elegido, detectara cual es
     * y nos lo comunicara
     * @return
     */
    private boolean hayCamposVacios() {
        if (ventana.tipoDepEstiloMaterialTxt.getText().isEmpty() ||
                ventana.txtReferencia.getText().isEmpty() ||
                ventana.txtSexo.getText().isEmpty() ||
                ventana.txtMarca.getText().isEmpty() ||
                ventana.txtPrecio.getText().isEmpty() ||
                ventana.txtColor.getText().isEmpty() ||
                ventana.fechaEntradaPicker.getText().isEmpty()) {
            return true;
        }
        return false;
    }

    /**
     * Metodo que una vez añadido un producto y dado de alta, los campos que han sido introducidos se borran.
     */
    private void limpiarCampos() {
        ventana.txtReferencia.requestFocus();
        ventana.txtSexo.setText(null);
        ventana.txtMarca.setText(null);
        ventana.txtColor.setText(null);
        ventana.fechaEntradaPicker.setText(null);
        ventana.txtPrecio.setText(null);
    }

    /**
     * Metodo que limpia la ventana, para que no se sobre escriba uno encima de otro, es decir, una vez
     * introducido un modelo no se borre por el siguiente modelo que se introducira
     */
    private void refrescar() {
        ventana.dlmZapateria.clear();
        for (Zapateria unZapato : modelo.obtenerZapatos()) {
            ventana.dlmZapateria.addElement(unZapato);
        }
    }


    /**
     * Metodo que limpia la clase lista de ventana, hace que no se sobrescriba la informacion de cada modelo
     */
    private void refrescarComboBox() {
        ventana.dcbm.removeAllElements();
        for (Zapateria unZapato : ventana.lista) {
            ventana.dcbm.addElement(unZapato);
        }
    }

    /**
     * Metodo que añade la accion en cada boton, para que visualmente se sepa cual se ha marcado.
     * @param listener
     */
    private void addActionListener(ActionListener listener) {
        ventana.deportivoBtn.addActionListener(listener);
        ventana.botasBtn.addActionListener(listener);
        ventana.mocasinBtn.addActionListener(listener);
        ventana.exportarBtn.addActionListener(listener);
        ventana.importarBtn.addActionListener(listener);
        ventana.nuevoBtn.addActionListener(listener);
    }

    /**
     * Metodo que añade la escucha de la ventana al panel de la ventana
     * @param listener
     */
    private void addWindowListener(WindowListener listener) {
        ventana.frame.addWindowListener(listener);
    }

    /**
     * Metodo que añade la visivilidad de la lista una vez se empiezan a añadir modelos de zapatos.
     * @param listener
     */
    private void addListSelectionListener(ListSelectionListener listener) {
        ventana.list1.addListSelectionListener(listener);
    }

    /**
     * Metodo que carga los datos en el fichero "Zapateria.conf, añadiendo la ultimaRutaExportada
     * @throws IOException
     */
    private void cargarDatosConfiguracion() throws IOException {
        Properties configuracion = new Properties();
        configuracion.load(new FileReader("Zapateria.conf"));
        ultimaRutaExportada = new File(configuracion.getProperty("ultimaRutaExportada"));
    }

    /**
     * Metodo que añade los nuevos datos introducidos en el fichero, ultimaRutaExportada
     * @param ultimaRutaExportada
     */
    private void actualizarDatosConfiguracion(File ultimaRutaExportada) {
        this.ultimaRutaExportada = ultimaRutaExportada;
    }

    /**
     * Metodo que guarda la configuracion del fichero y los datos añadidos en la ultimaRutaExportada
     * @throws IOException
     */
    private void guardarConfiguracion() throws IOException {
        Properties configuracion = new Properties();
        configuracion.setProperty("ultimaRutaExportada", ultimaRutaExportada.getAbsolutePath());
        configuracion.store(new PrintWriter("Zapateria.conf"), "Datos configuracion Zapateria");

    }


    /**
     * Metodo que devuelve los valores de cada parametro, dependiendo del tipo de modelo que se haya elegido.
     * Mostrara en la lista todos los datos de todos los modelos que se hayan introducido.
     * @param e
     */
    @Override
    public void valueChanged(ListSelectionEvent e) {
        if (e.getValueIsAdjusting()) {
            Zapateria zapatoSeleccionado = (Zapateria) ventana.list1.getSelectedValue();
            /**
             * Una vez asignado el valor, ya sea referencia, sexo, marca..., se devuelve el valor
             */
            ventana.txtReferencia.setText(String.valueOf(((Zapateria) zapatoSeleccionado).getReferencia()));
            ventana.txtSexo.setText(zapatoSeleccionado.getSexo());
            ventana.txtMarca.setText(zapatoSeleccionado.getMarca());
            ventana.comboBox1.setSelectedItem(String.valueOf((Zapateria) zapatoSeleccionado).toString());
            ventana.txtColor.setText(zapatoSeleccionado.getColor());
            ventana.fechaEntradaPicker.setDate(zapatoSeleccionado.getFechaEntrada());
            ventana.txtPrecio.setText(String.valueOf(((Zapateria) zapatoSeleccionado).getPrecio()));

            /**
             * dependiendo que boton se ha seleccionado, se devolvera un dato u otro.
             */
            if (zapatoSeleccionado instanceof Deportivas) {
                ventana.deportivoBtn.doClick();
                ventana.tipoDepEstiloMaterialTxt.setText(((Deportivas) zapatoSeleccionado).getTipoDeporte());
            }
            else if(zapatoSeleccionado instanceof Botas) {
                ventana.botasBtn.doClick();
                ventana.tipoDepEstiloMaterialTxt.setText(((Botas) zapatoSeleccionado).getEstilo());
            }
            else if(zapatoSeleccionado instanceof Mocasines) {
                ventana.mocasinBtn.doClick();
                ventana.tipoDepEstiloMaterialTxt.setText(((Mocasines) zapatoSeleccionado).getMaterial());
            }
        }
    }

    /**
     * Metodo que realiza la accion de una ventana auxiliar comunicando si se desea salir o no.
     * @param e
     */
    @Override
    public void windowClosing(WindowEvent e) {
        int resp = Util.mensajeConfirmacion("¿Desea salir de la ventana?", "Salir");
        if (resp == JOptionPane.OK_OPTION) {
            try {
                guardarConfiguracion();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            System.exit(0);
        }

    }

    /**
     * Metodo que muestra por consola que la ventana esta abierta
     * @param windowEvent
     */
    @Override
    public void windowOpened(WindowEvent windowEvent) {   }

    /**
     * Metodo que muestra por consola que la ventana esta cerrada
     * @param windowEvent
     */
    @Override
    public void windowClosed(WindowEvent windowEvent) { }

    /**
     * Metodo que muestra por consola que la ventana esta minimizada
     * @param windowEvent
     */
    @Override
    public void windowIconified(WindowEvent windowEvent) { }

    /**
     * Metodo que muestra por consola que la ventana esta maximizada
     * @param windowEvent
     */
    @Override
    public void windowDeiconified(WindowEvent windowEvent) { }

    /**
     * Metodo que muestra por consola que la ventana esta activada
     * @param windowEvent
     */
    @Override
    public void windowActivated(WindowEvent windowEvent) { }

    /**
     * Metodo que muestra por consola que la ventana esta desactivada
     * @param windowEvent
     */
    @Override
    public void windowDeactivated(WindowEvent windowEvent) {

    }


}
