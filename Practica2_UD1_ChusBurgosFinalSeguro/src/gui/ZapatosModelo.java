package gui;

import com.chusb.zapateria.base.Botas;
import com.chusb.zapateria.base.Deportivas;
import com.chusb.zapateria.base.Mocasines;
import com.chusb.zapateria.base.Zapateria;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.swing.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Objects;

import static java.lang.Float.parseFloat;
import static java.lang.Integer.parseInt;

public class ZapatosModelo {

    /**
     *  Creacion de un ArrayList de la clase zapateria, sera una lista donde se añadiran todos los parametros de esta clase.
     *
     */
    private ArrayList<Zapateria> listaZapatos;

    /**
     * Creacion del constructor del ArrayList para iniciarlo
     */
    public ZapatosModelo() {
        listaZapatos = new ArrayList<Zapateria>();
    }

    /**
     *  Metodo que devuelve los datos de la lista zapatos
     * @return listaZapatos
     */
    public ArrayList<Zapateria> obtenerZapatos() {
        return listaZapatos;
    }

    /**
     * Metodo que da de alta las deportivas, dentro de ella se crean los parametros asociados con la clase padre e hija,
     * se crea un objeto de la clase hija, nuevasDeportivas, se añade cada uno de los parametros. En la listaZapatos se añade las
     * nuevasDeportivas que se han dado de alta
     */
    public void altaDeportivas(int referencia, String sexo, String color, String marca, String talla, LocalDate fechaEntrada,
                               Float precio, String tipoDeporte) {
        Deportivas nuevasDeportivas = new Deportivas(referencia, sexo, marca, talla, color, fechaEntrada, precio, tipoDeporte);
        listaZapatos.add(nuevasDeportivas);
    }


    /**
     * Metodo que da de alta las botas, dentro de ella se crean los parametros asociados con la clase padre e hija,
     * se crea un objeto de la clase hija, nuevasBotas, se añade cada uno de los parametros. En la listaZapatos se añade las
     * nuevasdeportivas que se han dado de alta
     */
    public void altaBotas(int referencia, String sexo, String marca, String talla, String color, LocalDate fechaEntrada,
                          Float precio, String estilo) {
        Botas nuevasBotas = new Botas(referencia, sexo, marca, talla, color, fechaEntrada, precio, estilo);
        listaZapatos.add(nuevasBotas);
    }


    /**
     * Metodo que da de alta las mocasines, dentro de ella se crean los parametros asociados con la clase padre e hija,
     * se crea un objeto de la clase hija, nuevosMocasines, se añade cada uno de los parametros. En la listaZapatos se añade las
     * nuevosMocasines que se han dado de alta
     */
    public void altaMocasines(int referencia, String sexo, String marca, String talla, String color, LocalDate fechaEntrada,
                          Float precio, String material) {
        Mocasines nuevosMocasines = new Mocasines(referencia, sexo, marca, talla, color, fechaEntrada, precio, material);
        listaZapatos.add(nuevosMocasines);
    }

    /**
     * Metodo que devuelve un valor, comunincando que la referencia ya exite porque ya se ha introducido anteriormente.
     * Dentro de la lista zapateria se compararia la referencia introduciza, si el valor de la referencia ya existiese,
     * se tendria que añadir otro
     * @param referencia
     * @return true
     */
    public boolean existeReferencia(String referencia) {
        for (Zapateria unZapato : listaZapatos) {
            if(Objects.equals(unZapato.getReferencia(), referencia)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Metodo que exporta la informacion a un fichero XML.
     * @param fichero
     * @throws ParserConfigurationException
     * @throws TransformerException
     */

    public void exportarXML(File fichero) throws ParserConfigurationException, TransformerException {

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        DOMImplementation dom = builder.getDOMImplementation();
        Document documento = dom.createDocument(null, "xml", null);

        //Añado el nodo raiz - la primera etiqueta que contiene a las demás
        Element raiz = documento.createElement("Zapateria");
        documento.getDocumentElement().appendChild(raiz);

        Element nodoZapato = null, nodoDatos = null;
        Text texto = null;

        for (Zapateria unZapato : listaZapatos) {

            /* Añado dentro de la etiqueta raiz (Zapateria), una etiqueta
             * segun el tipo de zapato que este almacenando (deportivos, botas o mocasines)
             */

            if (unZapato instanceof Deportivas) {
                nodoZapato = documento.createElement("Deportivas");

            } else if(unZapato instanceof Botas) {
                nodoZapato = documento.createElement("Botas");
            }
            else {
                if(unZapato instanceof Mocasines) {
                    nodoZapato = documento.createElement("Mocasines");
                }
            }
            raiz.appendChild(nodoZapato);

            /* Dentro de la etiqueta zapateria le añado las subetiquetas con los datos de sus
             * atributos (referencia, modelo, marca...)
             */
            nodoDatos = documento.createElement("referencia");
            nodoZapato.appendChild(nodoDatos);
            texto = documento.createTextNode(String.valueOf(((Zapateria) unZapato).getReferencia()));
            nodoDatos.appendChild(texto);

            nodoDatos = documento.createElement("sexo");
            nodoZapato.appendChild(nodoDatos);
            texto = documento.createTextNode(unZapato.getSexo());
            nodoDatos.appendChild(texto);

            nodoDatos = documento.createElement("marca");
            nodoZapato.appendChild(nodoDatos);
            texto = documento.createTextNode(unZapato.getMarca());
            nodoDatos.appendChild(texto);

            nodoDatos = documento.createElement("talla");
            nodoZapato.appendChild(nodoDatos);
            texto = documento.createTextNode(String.valueOf(((Zapateria) unZapato).getTalla()));
            nodoDatos.appendChild(texto);

            nodoDatos = documento.createElement("color");
            nodoZapato.appendChild(nodoDatos);
            texto = documento.createTextNode(unZapato.getColor());
            nodoDatos.appendChild(texto);

            nodoDatos = documento.createElement("fecha-entrada");
            nodoZapato.appendChild(nodoDatos);
            texto = documento.createTextNode(unZapato.getFechaEntrada().toString());
            nodoDatos.appendChild(texto);

            nodoDatos = documento.createElement("precio");
            nodoZapato.appendChild(nodoDatos);
            texto = documento.createTextNode(String.valueOf(((Zapateria)unZapato).getPrecio()));
            nodoDatos.appendChild(texto);


            /* Como hay un dato que depende del tipo de Zapateria, debo acceder a él controlando el tipo de objeto(Deportivas,
            *  Botas o Mocasines)
            */
            if (unZapato instanceof Deportivas) {
                nodoDatos = documento.createElement("tipo-deportivas");
                nodoZapato.appendChild(nodoDatos);
                texto = documento.createTextNode(((Deportivas) unZapato).getTipoDeporte());
                nodoDatos.appendChild(texto);
            }
            else if(unZapato instanceof Botas) {
                nodoDatos = documento.createElement("estilo");
                nodoZapato.appendChild(nodoDatos);
                texto = documento.createTextNode(((Botas) unZapato).getEstilo());
                nodoDatos.appendChild(texto);
            }
            else {
                if(unZapato instanceof Mocasines) {
                    nodoDatos = documento.createElement("material");
                    nodoZapato.appendChild(nodoDatos);
                    texto = documento.createTextNode(((Mocasines) unZapato).getMaterial());
                    nodoDatos.appendChild(texto);
                }
            }
        }

        //Guardo los datos en "fichero" que es el objeto File recibido por parametro
        Source source = new DOMSource(documento);
        Result resultado = new StreamResult(fichero);

        Transformer transformer = TransformerFactory.newInstance().newTransformer();
        transformer.transform(source, resultado);

    }

    /**
     * Metodo que importa los datos a un XML.
     * @param fichero
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SAXException
     */
    public void importarXML(File fichero) throws ParserConfigurationException, IOException, SAXException {

        //Se crean objetos de las clases hijas para darles el valor de cada parametro
        listaZapatos = new ArrayList<Zapateria>();
        Deportivas nuevasDeportivas = null;
        Botas nuevasBotas = null;
        Mocasines nuevosMocasines = null;

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document documento = builder.parse(fichero);

        //Añado el nodo lista - la primera lista que contiene a las demás
        NodeList listaElementos = documento.getElementsByTagName("*");

        /* Se recorre un for de la lista elementos y añado el nodo Element para comprar los valores, y mostrar un tipo de
        * zapato u otro
        */
        for (int i = 0; i < listaElementos.getLength(); i++) {
            Element nodoZapateria = (Element) listaElementos.item(i);
            /*
             * Dependiendo del tipo de modelo que se haya elegido, se van asignando los valores a cada uno de los parametros
             * diferenciandolos de los otros modelos, el tipoDeporte, estilo o material.
             * Una vez esto, se añade a la lista de zapatos las nuevasDeportivas, las nuevasBotas o los nuevosMocasines
             */
            if (nodoZapateria.getTagName().equals("Deportivas")) {
                nuevasDeportivas = new Deportivas();
                nuevasDeportivas.setReferencia(parseInt(nodoZapateria.getChildNodes().item(0).getTextContent()));
                nuevasDeportivas.setSexo(nodoZapateria.getChildNodes().item(1).getTextContent());
                nuevasDeportivas.setMarca(nodoZapateria.getChildNodes().item(2).getTextContent());
                nuevasDeportivas.setTalla(nodoZapateria.getChildNodes().item(3).getTextContent());
                nuevasDeportivas.setColor(nodoZapateria.getChildNodes().item(4).getTextContent());
                nuevasDeportivas.setFechaEntrada(LocalDate.parse(nodoZapateria.getChildNodes().item(5).getTextContent()));
                nuevasDeportivas.setTipoDeporte(nodoZapateria.getChildNodes().item(6).getTextContent());
                nuevasDeportivas.setPrecio(parseFloat(nodoZapateria.getChildNodes().item(7).getTextContent()));

                listaZapatos.add(nuevasDeportivas);

            }
            else if (nodoZapateria.getTagName().equals("Botas")) {
                nuevasBotas = new Botas();
                nuevasBotas.setReferencia(parseInt(nodoZapateria.getChildNodes().item(0).getTextContent()));
                nuevasBotas.setSexo(nodoZapateria.getChildNodes().item(1).getTextContent());
                nuevasBotas.setMarca(nodoZapateria.getChildNodes().item(2).getTextContent());
                nuevasBotas.setTalla(nodoZapateria.getChildNodes().item(3).getTextContent());
                nuevasBotas.setColor(nodoZapateria.getChildNodes().item(4).getTextContent());
                nuevasBotas.setFechaEntrada(LocalDate.parse(nodoZapateria.getChildNodes().item(5).getTextContent()));
                nuevasBotas.setEstilo(nodoZapateria.getChildNodes().item(6).getTextContent());
                nuevasBotas.setPrecio(parseFloat(nodoZapateria.getChildNodes().item(7).getTextContent()));

                listaZapatos.add(nuevasBotas);
            }
            else {
                if(nodoZapateria.getTagName().equals("Mocasines")) {
                    nuevosMocasines = new Mocasines();
                    nuevosMocasines.setReferencia(parseInt(nodoZapateria.getChildNodes().item(0).getTextContent()));
                    nuevosMocasines.setSexo(nodoZapateria.getChildNodes().item(1).getTextContent());
                    nuevosMocasines.setMarca(nodoZapateria.getChildNodes().item(2).getTextContent());
                    nuevosMocasines.setTalla(nodoZapateria.getChildNodes().item(3).getTextContent());
                    nuevosMocasines.setColor(nodoZapateria.getChildNodes().item(4).getTextContent());
                    nuevosMocasines.setFechaEntrada(LocalDate.parse(nodoZapateria.getChildNodes().item(5).getTextContent()));
                    nuevosMocasines.setMaterial(nodoZapateria.getChildNodes().item(6).getTextContent());
                    nuevosMocasines.setPrecio(parseFloat(nodoZapateria.getChildNodes().item(7).getTextContent()));

                    listaZapatos.add(nuevosMocasines);
                }
            }
        }


    }


}

