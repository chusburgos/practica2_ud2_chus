package com.chusb.zapateria.util;

import gui.Ventana;
import gui.ZapatosControlador;
import gui.ZapatosModelo;

/**
 * @author Chus Burgos
 * @version java version 1.8.0_111
 */


public class Principal {
    /**
     * El main crea el objeto ventana y el objeto modelo, de las clases Ventana y ZapatosModelos, y a su vez tambien
     * se crea el objeto controlador que se pasa por parametros la ventana y el modelo. Esto hara que cuando se ejecute
     * la aplicacion añada la clase ventana y modelo.
     */
    public static void main(String[] args) {
        Ventana ventana = new Ventana();
        ZapatosModelo modelo = new ZapatosModelo();
        ZapatosControlador controlador = new ZapatosControlador(ventana, modelo);

    }
}
